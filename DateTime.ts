export class DateTime
{
    public day: number;
    public month: number;
    public year: number;

    public minutesSinceMidnight: number;

    public constructor(day: number, month: number, year: number, minutesSinceMidnight: number)
    {
        this.day = day;
        this.month = month;
        this.year = year;
        this.minutesSinceMidnight = minutesSinceMidnight;
    }
    
    public addMinutes(minutes: number): DateTime
    {
        this.minutesSinceMidnight = this.minutesSinceMidnight + minutes;
        return this;
    }
    
    public addDays(count: number): DateTime
    {
        let day = this.day;
        let month = this.month;
        let year = this.year;
        let nodim: number = DateTime.getNumberOfDaysInMonth(this.month, this.year);
        if (count <= 27)
        {
            if ((day + count) <= nodim)
                day = day + count;
            else
            {
                day = count - (nodim - day);
                if (month < 12)
                    month++;
                else {
                    month = 1;
                    year++;
                }
            }
        }
        else 
        {
            console.log("can't add more than number of days in this month");
        }
        return new DateTime(day, month, year, this.minutesSinceMidnight);
    }
    
    public subtractDays(count: number): DateTime
    {
        let day = this.day;
        let month = this.month;
        let year = this.year;
        let nodim: number = DateTime.getNumberOfDaysInMonth(this.month > 1 ? this.month-1 : 12, this.year);
        let result = day - count;
        if (count <= nodim)
        {
            if (result > 0)
                day = result;
            else
            {
                day = nodim + result;
                if (month > 1)
                    month--;
                else {
                    month = 1;
                    year--;
                }
            }
        }
        else 
        {
            console.log("can't substract more than number of days in this month");
        }
        return new DateTime(day, month, year, this.minutesSinceMidnight);
    }
    
    public addMonths(count: number): DateTime
    {
        let newDate: Date = this.getDateObject();
        newDate.setMonth(newDate.getMonth() + count)
        return new DateTime(newDate.getUTCDate(), newDate.getUTCMonth() + 1, newDate.getUTCFullYear(), 0);
    }
    
    public subtractMonths(count: number): DateTime
    {
        return this.addMonths(0-count);
    }
    
    public getDateObject(): Date
    {
        return new Date(Date.UTC(this.year, this.month-1, this.day, (this.minutesSinceMidnight/60)-this.minutesSinceMidnight%60, this.minutesSinceMidnight%60));
    }
    
    public static createFromDate(date: Date): DateTime
    {
        return new DateTime(date.getUTCDate(), date.getUTCMonth() + 1, date.getUTCFullYear(), date.getHours() * 60 + date.getUTCMinutes());
    }
    
    public static getNumberOfDaysInMonth(m: number, y: number): number
    {
        return m === 2 ? y & 3 || !(y % 25) && y & 15 ? 28 : 29 : 30 + (m + (m >> 3) & 1);
    }

    public get machineDateAsString(): string
    {
        return (
            this.year + "-" +
            (this.month.toString().length == 2 ? this.month : "0" + this.month) + "-" +
            (this.day.toString().length == 2 ? this.day : "0" + this.day)
        );
    }

    public get machineDateTimeAsString(): string
    {
        var minutes = this.minutesSinceMidnight % 60;
        var hours = (this.minutesSinceMidnight - minutes) / 60;
        return (
            this.year + "-" +
            (this.month.toString().length == 2 ? this.month : "0" + this.month) + "-" +
            (this.day.toString().length == 2 ? this.day : "0" + this.day) + " " +
            (hours.toString().length == 2 ? hours : "0" + hours) + ":" +
            (minutes.toString().length == 2 ? minutes : "0" + minutes) 
        );
    }

    public get ordinalDay(): string
    {
        let dayString = this.day.toString();
        let string: string;
        switch (dayString.substr(dayString.length-1)) 
        {
            case "1":
                string = "st";
                break;
        
            case "2":
                string = "nd";
                break;
        
            case "3":
                string = "rd";
                break;
        
            default:
                string = "th";
                break; 
        }
        return (dayString + string);
    }

    public get dateAsString(): string
    {
        return this.monthAsString + " " + this.day + ", " + this.year;
    }

    public get shortDateAsString(): string
    {
        return this.day + "." + this.month + "." + this.year;
    }

    public get dateTimeAsString(): string
    {
        return this.dateAsString + " " + this.timeAsString;
    }

    public get monthAsString(): string
    {
        switch (this.month)
        {
            case 1:
                return "January";
 
            case 2:
                return "February";

            case 3:
                return "March";

            case 4:
                return "April";

            case 5:
                return "May";

            case 6:
                return "June";

            case 7:
                return "July";

            case 8:
                return "August";

            case 9:
                return "September";

            case 10:
                return "October";

            case 11:
                return "November";

            case 12:
                return "December";
        }
    }
    
    public get dayInWeekAsString(): string
    {
        switch ((this.getDateObject() as Date).getUTCDay())
        {
            case 0:
                return "Su";
 
            case 1:
                return "Mo";

            case 2:
                return "Tu";

            case 3:
                return "We";

            case 4:
                return "Th";

            case 5:
                return "Fr";
                
            case 6:
                return "Sa";
        }
    }

    public get timeAsString(): string
    {
        var minute = this.minutesSinceMidnight % 60;
        var hour = (this.minutesSinceMidnight - minute) / 60;
        var phase = "AM";
        var nextDay = false;

        if (this.minutesSinceMidnight >= 1440)
        {
            nextDay = true;
            hour -= 24;

            if (hour == 0)
            {
                hour = 12;
                phase = "AM";
            }
            else
            {
                if (hour >= 12)
                {
                    phase = "PM";

                    if (hour > 12)
                        hour -= 12;
                }
            }
        }
        else
        {
            if (hour == 0)
            {
                hour = 12;
                phase = "AM";
            }
            else
            {
                if (hour >= 12)
                {
                    phase = "PM";

                    if (hour > 12)
                        hour -= 12;
                }
            }
        }

        return hour + ":" + (minute < 10 ? "0" + minute : minute) + " " + phase;
    }
    
    public static machineDateToDateTime(d: string): DateTime // eg. "2016-04-16 09:15" // TODO rename to machineDateTimeToDateTime()
    {
        let x = d.split(" "); 
        let date = x[0].split("-");
        let time = x[1].split(":");
        let result = new DateTime(parseInt(date[2]), parseInt(date[1]), parseInt(date[0]), parseInt(time[0])*60 + parseInt(time[1]));
        return result;
    }
    
    public static timestamp(time: number): string // from minutes since midnight
    {
        let t = time > 1440 ? time - 1440 : time;
        let m = t%60;
        let h = (t-m)/60;
        return h + ":" + ( m.toString().length == 1 ? "0" + m : m );
    }
    
    public static now(): DateTime
    {
        let today = new Date();
        return (new DateTime(today.getUTCDate(), today.getUTCMonth()+1, today.getUTCFullYear(), (today.getUTCHours() * 60) + today.getUTCMinutes() ));
    }
    
    public static today(): DateTime
    {
        let today = new Date();
        return (new DateTime(today.getUTCDate(), today.getUTCMonth()+1, today.getUTCFullYear(), 0));
    }
}
