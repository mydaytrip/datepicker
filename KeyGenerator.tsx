export class KeyGenerator 
{ 
    protected static last: number = 0; 
    
    public static next(): number
    { 
        this.last += 1; 
        return this.last; 
    } 
}