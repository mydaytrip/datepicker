/// <reference path="../../../typings/index.d.ts" />

import React = require("react");
import Immutable = require("immutable");

import { KeyGenerator } from "./KeyGenerator";
import { DateTime } from "./DateTime";


class DayCell extends React.Component<any, any>
{
    protected onClick(): void
    {
        this.props.calendar.onDateChange(this.props.calendar, this.props.represents);
    }

    public render(): JSX.Element
    {
        return (
            <td
                className={ this.props.selected ? "selected" : "" }
                onClick={ this.onClick.bind(this) }>
                {this.props.represents.day}
            </td>
        );
    }
}
 
class MonthView extends React.Component<any, any>
{
    protected get numberOfDays(): number
    {
        let m = this.props.displayed.month;
        let y = this.props.displayed.year;
        return m === 2 ? y & 3 || !(y % 25) && y & 15 ? 28 : 29 : 30 + (m + (m >> 3) & 1);
    }

    public render(): JSX.Element
    {
        return (
            <table>
                <thead>
                    <tr>
                        <th>S</th>
                        <th>M</th>
                        <th>T</th>
                        <th>W</th>
                        <th>T</th>
                        <th>F</th>
                        <th>S</th>
                    </tr>
                </thead>

                <tbody>
                    {
                        (() =>
                        {
                            let output = [];

                            // find out which day of week is 1st
                            let offset: number = (new Date(this.props.displayed.year, this.props.displayed.month - 1, 1)).getDay();
                            let minutes = this.props.displayed.minutesSinceMidnight%60;
                            let hours = (this.props.displayed.minutesSinceMidnight-minutes)/60;
                            let day = 1;
                            let iter = 1;

                            for (let i = 0; i <= Math.ceil((this.numberOfDays + offset) / 7); i++)
                            {
                                let partialOutput = [];
                                let startOfWeek = iter;
                                let dayInWeek = 0;

                                for (; iter <= startOfWeek + 6;)
                                {
                                    if (day == 1 && offset != 0)
                                    {
                                        partialOutput.push(<td key={ KeyGenerator.next() }></td>);
                                        offset--;
                                        iter++;
                                        continue;
                                    }

                                    if (day > this.numberOfDays)
                                    {
                                        if (dayInWeek == 7)
                                            break;
                                        else
                                        {
                                            partialOutput.push(<td key={ KeyGenerator.next() }></td>);
                                            day++;
                                            iter++;
                                            dayInWeek++;
                                            continue;
                                        }
                                    }

                                    let selected = this.props.selected.day == day && this.props.selected.month == this.props.displayed.month && this.props.selected.year == this.props.displayed.year;
                                    if (this.props.startDate != null)
                                    {
                                        if ((new DateTime(day, this.props.displayed.month, this.props.displayed.year, (hours * 60) + minutes)).getDateObject().getTime() > this.props.startDate.getDateObject().getTime())
                                        {
                                            partialOutput.push(<DayCell key={ KeyGenerator.next() } selected={selected} represents={new DateTime(day, this.props.displayed.month, this.props.displayed.year, this.props.selected.minutesSinceMidnight)} calendar={this.props.calendar} />);
                                        }
                                        else
                                            partialOutput.push(<td className={ selected ? "disabled selected" : "disabled" } key={ KeyGenerator.next() }>{ day }</td>);
                                    }
                                    else
                                        partialOutput.push(<DayCell key={ KeyGenerator.next() } selected={selected} represents={new DateTime(day, this.props.displayed.month, this.props.displayed.year, this.props.selected.minutesSinceMidnight)} calendar={this.props.calendar} />);

                                    day++;
                                    iter++;
                                    dayInWeek++;
                                }

                                output.push(<tr key={ KeyGenerator.next() }>{partialOutput}</tr>);
                            }

                            return output;
                        })()
                    }
                </tbody>
            </table>
        );
    }
}

class TimeView extends React.Component<any, any>
{
    protected getNumberOfDays(m: number, y: number): number
    {
        return m === 2 ? y & 3 || !(y % 25) && y & 15 ? 28 : 29 : 30 + (m + (m >> 3) & 1);
    }
    
    protected isInLimit(value: number): boolean // in minutes
    {
        let limit: DateTime = this.props.startDate;
        let allMinutes = this.props.selected.minutesSinceMidnight + value;
        let m = allMinutes%60;
        let h = (allMinutes - m)/60;
        
        let selectedDateTime = new DateTime(this.props.selected.day, this.props.selected.month, this.props.selected.year, allMinutes);
        if (limit == null)
            return true;
        if (selectedDateTime.getDateObject().getTime() > limit.getDateObject().getTime())
            return true;
        return false;
    }

    protected decrementHour(): void
    {
        if (this.isInLimit(-60))
            this.props.calendar.onDateChange(this.props.calendar, new DateTime(this.props.selected.day, this.props.selected.month, this.props.selected.year, 
            (this.props.selected.minutesSinceMidnight < 60 ? 
            this.props.selected.minutesSinceMidnight - 60 + 720 : 
            this.props.selected.minutesSinceMidnight - 60)));
    }

    protected incrementHour(): void
    {
        if (this.isInLimit(60))
            this.props.calendar.onDateChange(this.props.calendar, new DateTime(this.props.selected.day, this.props.selected.month, this.props.selected.year, 
            (this.props.selected.minutesSinceMidnight >= 1380 ? 
            this.props.selected.minutesSinceMidnight + 60 - 720 : 
            this.props.selected.minutesSinceMidnight + 60)));
    }

    protected decrementMinutes(): void
    {
        if (this.isInLimit(-15))
            this.props.calendar.onDateChange(this.props.calendar, new DateTime(this.props.selected.day, this.props.selected.month, this.props.selected.year, this.props.selected.minutesSinceMidnight % 60 == 0 ? this.props.selected.minutesSinceMidnight + 45 : (this.props.selected.minutesSinceMidnight < 15 ? this.props.selected.minutesSinceMidnight - 15 + 1440 : this.props.selected.minutesSinceMidnight - 15)));
    }

    protected incrementMinutes(): void
    {
        if (this.isInLimit(15))
            this.props.calendar.onDateChange(this.props.calendar, new DateTime(this.props.selected.day, this.props.selected.month, this.props.selected.year, 
            (this.props.selected.minutesSinceMidnight - Math.floor(this.props.selected.minutesSinceMidnight / 60) * 60) == 45 ? this.props.selected.minutesSinceMidnight - 45 : (this.props.selected.minutesSinceMidnight >= 1425 ? this.props.selected.minutesSinceMidnight + 15 - 1440 : this.props.selected.minutesSinceMidnight + 15)));
    }

    protected togglePhase(): void
    {
        if (this.isInLimit( this.props.selected.minutesSinceMidnight >= 720 ? -720 : 720 ))
        {
            let realHours: number = Math.floor(this.props.selected.minutesSinceMidnight / 60);
            let phase: string = realHours >= 12 && realHours != 24 ? "PM" : "AM";

            if (phase == "AM")
                this.props.calendar.onDateChange(this.props.calendar, new DateTime(this.props.selected.day, this.props.selected.month, this.props.selected.year, this.props.selected.minutesSinceMidnight + 720));
            else
                this.props.calendar.onDateChange(this.props.calendar, new DateTime(this.props.selected.day, this.props.selected.month, this.props.selected.year, this.props.selected.minutesSinceMidnight - 720));
        }
    }

    public render(): JSX.Element
    {
        let realHours: number = Math.floor(this.props.selected.minutesSinceMidnight / 60);
        let phase: string = realHours >= 12 && realHours != 24 ? "PM" : "AM";
        let minutes: number = this.props.selected.minutesSinceMidnight - (realHours * 60);

        let hours = realHours > 12 ? realHours - 12 : realHours;

        let minutesView = (
            <div className="timeMinutes">
                <button type="button" onClick={this.decrementMinutes.bind(this)}>{minutes == 0 ? 45 : minutes == 15 ? "00" : minutes - 15}</button>
                <span>{minutes == 0 ? "00" : minutes}</span>
                <button type="button" onClick={this.incrementMinutes.bind(this)}>{minutes == 45 ? "00" : minutes + 15}</button>
            </div>
        );

        let phaseView: JSX.Element;
        if (phase == "AM")
            phaseView = (
                <div className="timePhase">
                    <button type="button" className="selected" onClick={this.togglePhase.bind(this)}>AM</button>
                </div>
            );
        else
            phaseView = (
                <div className="timePhase">
                    <button type="button" className="selected" onClick={this.togglePhase.bind(this)}>PM</button>
                </div>
            );

        if (this.props.selected.minutesSinceMidnight < 60)
        {
            return (
                <div className={ this.props.type == "time" ? "time timePicker timeOnly" : "time timePicker" }>
                    <div className="timeHours">
                        <button type="button" onClick={this.decrementHour.bind(this)}>11</button>
                        <span>12 <span className="timeDivider">:</span></span>
                        <button type="button" onClick={this.incrementHour.bind(this)}>1</button>
                    </div>

                    {minutesView}

                    {phaseView}

                    <button type="button" className="doneButton" onClick={ this.props.onClickOutside }>Done</button>
                </div>
            );
        }
        return (
            <div className={ this.props.type == "time" ? "time timePicker timeOnly" : "time timePicker" }>
                <div className="timeHours">
                    <button type="button" onClick={this.decrementHour.bind(this)}>{hours == 1 ? 12 : hours - 1}</button>
                    <span>{hours} <span className="timeDivider">:</span></span>
                    <button type="button" onClick={this.incrementHour.bind(this)}>{hours == 12 ? 1 : hours + 1}</button>
                </div>

                {minutesView}

                {phaseView}

                <button type="button" className="doneButton" onClick={ this.props.onClickOutside }>Done</button>
            </div>
        );
    }
}

interface ICalendarProps
{
    callback: (datetime: DateTime) => void;
    onClickOutside: () => void;
    selected: DateTime;
    isClearable?: boolean;
    type?: string; 
    startDate?: DateTime;
    className?: String;
}

export class Calendar extends React.Component<ICalendarProps, any>
{
    public static get TYPE_TIME(): string {return "time";}
    public static get TYPE_DATE(): string {return "date";}

    public defaultProps: ICalendarProps = {
        callback: null,
        onClickOutside: null,
        selected: null,
        isClearable: false,
        type: null, 
        startDate: null,
        className: ""
    };
    
    public state = { displayed: this.props.selected != null ? this.props.selected : DateTime.now() , selected: this.props.selected != null ? this.props.selected : DateTime.now(), view: false };

    protected boundListener = null;

    public componentWillMount(): void
    {
        let listener = this.handleDocumentClick.bind(this);
        this.boundListener = listener;
        document.getElementById("page").addEventListener("click", listener);
    }

    public componentWillUnmount(): void
    {
        document.getElementById("page").removeEventListener("click", this.boundListener);
    }

    protected handleDocumentClick(e): void
    {
        let pickers = document.getElementsByClassName("datetimePicker");
        let isNotCalendar = true;
        for (let i = 0; i < pickers.length; i++)
        {
            if ((pickers[i]).contains(e.target as HTMLElement) && isNotCalendar)
                isNotCalendar = false;
        }
        if (
            (e.target as HTMLElement).className != "datetimeToggleButton" && 
            (e.target as HTMLElement).className != "time" && 
            (e.target as HTMLElement).className != "displayedStartPicker" && 
            isNotCalendar
        )
            this.props.onClickOutside();
    }

    protected onDateChange(reference: Calendar, newDate: DateTime): void
    {
        this.setState({ displayed: (newDate.day != null  ? newDate : this.state.displayed), selected: newDate, view: this.state.view });
        this.props.callback(newDate);
    }

    protected onViewToggle(): void
    {
        this.setState({ displayed: this.state.selected, selected: this.state.selected, view: !this.state.view });
    }

    protected onPreviousClicked(): void
    {
        let newDisplayed: DateTime;

        if (this.state.displayed.month == 1)
            newDisplayed = new DateTime(this.state.displayed.day, 12, this.state.displayed.year - 1, this.state.displayed.minutesSinceMidnight);
        else
            newDisplayed = new DateTime(this.state.displayed.day, this.state.displayed.month - 1, this.state.displayed.year, this.state.displayed.minutesSinceMidnight);

        this.setState({ displayed: newDisplayed, selected: this.state.selected });
    }

    protected onNextClicked(): void
    {
        let newDisplayed: DateTime;

        if (this.state.displayed.month == 12)
            newDisplayed = new DateTime(this.state.displayed.day, 1, this.state.displayed.year + 1, null);
        else
            newDisplayed = new DateTime(this.state.displayed.day, this.state.displayed.month + 1, this.state.displayed.year, null);

        this.setState({ displayed: newDisplayed, selected: this.state.selected });
    }

    public render(): JSX.Element
    {
        if (!this.state.view && this.props.type != "time")
            return (
                <div className={ this.props.className + " " + this.props.type }>
                    <div className="monthHeading">
                        <button type="button" className="previousMonth" onClick={this.onPreviousClicked.bind(this)}></button>

                        <span className="month-name">{this.state.displayed.monthAsString} {this.state.displayed.year}</span>

                        <button type="button" className="nextMonth" onClick={this.onNextClicked.bind(this)}></button>
                    </div>
                    
                    <MonthView startDate={ this.props.startDate } displayed={this.state.displayed} selected={this.state.selected} calendar={this} />
                    <div className="datetimePickerActions">
                        {
                            this.props.isClearable ?
                            <button type="button" className="clearButtonMonth" onClick={ this.onDateChange.bind(this, {}) }>Clear</button>
                            :
                            null
                        }
                        <button type="button" className="doneButtonMonth" onClick={ this.props.onClickOutside }>Done</button>
                    </div>
                    {
                        (() => {
                            if (this.props.type != "date")
                                return (
                                    <div className="time" onClick={this.onViewToggle.bind(this)}><span style={{ background: "#FAFAFA", paddingLeft: "7px", paddingRight: "7px", borderRadius: "8px" }}>{this.state.selected.timeAsString}</span></div>
                                ); 
                        })()
                    }
                </div>
            );
        else
            return (
                <div className={ this.props.className + " " + this.props.type }>
                    {
                        (() => {
                            if (this.props.type != "time")
                                return (
                                    <div className="monthHeading clickable" onClick={this.onViewToggle.bind(this)}>
                                        {this.state.selected.dateAsString}
                                    </div>
                                );
                        })()
                    }
                    
                    <TimeView startDate={ this.props.startDate } onClickOutside={ this.props.onClickOutside } displayed={this.state.displayed} selected={this.state.selected} calendar={this} type={ this.props.type } />
                </div>
            );
    }
}