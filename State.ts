/// <reference path="../../../typings/index.d.ts" />

import React = require("react");
import Immutable = require("immutable");

export class State 
{
    public data: Immutable.Map<string, any>;
    
    public constructor(data: Immutable.Map<string, any>)
    {
        this.data = data;
    }
}